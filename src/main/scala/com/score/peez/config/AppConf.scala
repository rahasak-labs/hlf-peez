package com.score.peez.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

/**
  * Load configurations define in application.conf from here
  *
  * @author eranga herath(erangaeb@gmail.com)
  */
trait AppConf {
  // config object
  val appConf = ConfigFactory.load()

  // senzie config
  lazy val senzieMode = Try(appConf.getString("senzie.mode")).getOrElse("DEV")
  lazy val senzieName = Try(appConf.getString("senzie.name")).getOrElse("sampath.miner")

  // keys config
  lazy val keysDir = Try(appConf.getString("keys.dir")).getOrElse(".keys")
  lazy val publicKeyLocation = Try(appConf.getString("keys.public-key-location")).getOrElse(".keys/id_rsa.pub")
  lazy val privateKeyLocation = Try(appConf.getString("keys.private-key-location")).getOrElse(".keys/id_rsa")
}
