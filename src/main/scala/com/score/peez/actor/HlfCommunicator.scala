package com.score.peez.actor

import akka.actor.{Actor, Props}
import com.score.peez.config.AppConf
import com.score.peez.hlf.HlfFactory
import org.hyperledger.fabric.sdk.HFClient

object HlfCommunicator {

  case class Init()

  case class ExecQuery()

  case class ExecInvoke()

  case class FloodQuery()

  case class FloodInvoke()

  def props = Props(classOf[HlfCommunicator])

  var client: HFClient = _

}

class HlfCommunicator extends Actor with AppConf {

  import HlfCommunicator._

  override def receive: Receive = {
    case Init =>
      // enroll admin/user with CA
      val caClient = HlfFactory.initCaClient()
      val admin = HlfFactory.enrollAdmin(caClient)
      val user = HlfFactory.enrollUser(caClient, admin, "org1usr1")

      // setup client with user
      client = HlfFactory.initClient
      client.setUserContext(user)

      // setup channel and exec query
      HlfFactory.initChannel(client)
      self ! ExecInvoke
    case ExecQuery =>
      // execute query
      HlfFactory.execQuery(client)
    case ExecInvoke =>
      // execute invoke
      HlfFactory.execInvoke(client)
    case FloodQuery =>
      // flood query
      HlfFactory.floodQuery(client)
    case FloodInvoke =>
      // flood invoke
      HlfFactory.floodInvoke(client)
  }
}
