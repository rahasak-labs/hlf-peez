package com.score.peez

import akka.actor.ActorSystem
import com.score.peez.actor.HlfCommunicator
import com.score.peez.actor.HlfCommunicator.Init
import com.score.peez.util.UserStore

object Main extends App {

  // first setup user store
  UserStore.init()

  // start hlf communicator
  implicit val system = ActorSystem("octopus")
  system.actorOf(HlfCommunicator.props, name = "HlfExecutor") ! Init

}
