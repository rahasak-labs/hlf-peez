package com.score.peez.util

import java.io.{File, ObjectInputStream, ObjectOutputStream}
import java.nio.file.{Files, Paths}

import com.score.peez.config.AppConf
import com.score.peez.hlf.PeezUser

import scala.util.{Failure, Success, Try}


object UserStore extends AppConf {
  def init() = {
    // first create .keys directory
    val dir: File = new File(keysDir)
    if (!dir.exists) {
      dir.mkdir
    }
  }

  def put(user: PeezUser): Unit = {
    val out = new ObjectOutputStream(Files.newOutputStream(Paths.get(s"$keysDir/${user.getName}")))
    out.writeObject(user)

    println(s"Created user ${user.getName}")
  }

  def get(name: String): Option[PeezUser] = {
    Try {
      val in = new ObjectInputStream(Files.newInputStream(Paths.get(s"$keysDir/$name")))
      in.readObject().asInstanceOf[PeezUser]
    } match {
      case Success(u) =>
        println(s"Found with $name")
        Option(u)
      case Failure(e) =>
        println(s"No user found with $name, got error ${e.getStackTrace}")
        None
    }
  }

}

