package com.score.peez.hlf

import java.util.function.BiConsumer

import com.score.peez.util.UserStore
import org.hyperledger.fabric.sdk.security.CryptoSuite
import org.hyperledger.fabric.sdk.{BlockEvent, ChaincodeID, Channel, HFClient}
import org.hyperledger.fabric_ca.sdk.{HFCAClient, RegistrationRequest}

import scala.collection.JavaConverters._
import scala.concurrent.duration._
import scala.util.Random

object HlfFactory {

  def initCaClient(): HFCAClient = {
    val cryptoSuite = CryptoSuite.Factory.getCryptoSuite()
    val caClient = HFCAClient.createNewInstance("http://10.252.94.61:7054", null)
    caClient.setCryptoSuite(cryptoSuite)

    caClient
  }

  def initClient: HFClient = {
    val cryptoSuite = CryptoSuite.Factory.getCryptoSuite
    val client = HFClient.createNewInstance()
    client.setCryptoSuite(cryptoSuite)

    client
  }

  def initChannel(client: HFClient): Channel = {
    // new
    //  1. peer
    //  2. eventhub
    //  3. orderer
    val peer = client.newPeer("peer0.org1.example.com", "grpc://10.252.94.61:7051")
    val eventHub = client.newEventHub("eventhub01", "grpc://10.252.94.61:7053")
    val orderer = client.newOrderer("orderer0.example.com", "grpc://10.252.94.61:7050")

    // setup channel
    val channel = client.newChannel("mychannel")
    channel.addPeer(peer)
    channel.addEventHub(eventHub)
    channel.addOrderer(orderer)
    channel.initialize()
    channel
  }

  def enrollAdmin(caClient: HFCAClient) = {
    // check enrolled admin first
    UserStore.get("admin") match {
      case Some(admin) =>
        admin
      case None =>
        // admin credentials defined in docker-compose(ca service `command` section)
        val enrollment = caClient.enroll("admin", "adminpw")
        val admin = PeezUser("admin", "admin", "org1", "Org1MSP", enrollment)
        UserStore.put(admin)

        admin
    }
  }

  def enrollUser(caClient: HFCAClient, admin: PeezUser, userId: String) = {
    UserStore.get(userId) match {
      case Some(user) =>
        user
      case None =>
        val rr = new RegistrationRequest(userId, "org1")
        val enrollmentSecret = caClient.register(rr, admin)
        val enrollment = caClient.enroll(userId, enrollmentSecret)
        val user = PeezUser(userId, userId, "org1", "Org1MSP", enrollment)
        UserStore.put(user)

        user
    }
  }

  def execQuery(client: HFClient): Unit = {
    val channel = client.getChannel("mychannel")

    // create cc request
    val qpr = client.newQueryProposalRequest()

    // build cc id providing the chaincode name. Version is omitted here.
    val cid = ChaincodeID.newBuilder().setName("mycc").build()
    qpr.setChaincodeID(cid)

    // cc function to be called
    qpr.setFcn("get")
    qpr.setArgs("a")

    val res = channel.queryByChaincode(qpr).asScala

    // result
    res.foreach { r =>
      println(new String(r.getChaincodeActionResponsePayload))
    }
  }

  def execInvoke(client: HFClient): Unit = {
    val channel = client.getChannel("mychannel")

    // transaction proposal
    val tpr = client.newTransactionProposalRequest()
    val cid = ChaincodeID.newBuilder().setName("mycc").build()
    tpr.setChaincodeID(cid)

    // cc function and args
    tpr.setFcn("set")
    tpr.setArgs("a", Random.nextInt(100).toString)

    // send trans proposal
    val res = channel.sendTransactionProposal(tpr)
    if (res.asScala.count(p => p.isInvalid) > 0) {
      println("Error proposal response")
    } else {
      channel.sendTransaction(res)

      // handle invoke response
      val event = channel.sendTransaction(res)
      event.whenCompleteAsync {
        new BiConsumer[BlockEvent#TransactionEvent, Throwable] {
          override def accept(t: BlockEvent#TransactionEvent, u: Throwable): Unit = {
            if (t.isValid) {
              println(s"Success response for ${t.getTransactionID}")
            } else {
              println(s"Error response for ${t.getTransactionID}")
            }
          }
        }
      }
    }
  }

  def floodQuery(client: HFClient): Unit = {
    var i = 0

    val deadline = 30.seconds.fromNow
    while (deadline.hasTimeLeft) {
      val channel = client.getChannel("mychannel")

      // create cc request
      val qpr = client.newQueryProposalRequest()

      // build cc id providing the chaincode name. Version is omitted here.
      val cid = ChaincodeID.newBuilder().setName("mycc").build()
      qpr.setChaincodeID(cid)

      // cc function to be called
      qpr.setFcn("get")
      qpr.setArgs("a")

      val res = channel.queryByChaincode(qpr).asScala

      // result
      res.foreach { r =>
        println(new String(r.getChaincodeActionResponsePayload))
      }

      i = i + 1
    }

    println(s"Success - $i")
  }

  def floodInvoke(client: HFClient): Unit = {
    var i, j = 0

    // run for 30 seconds
    val deadline = 30.seconds.fromNow
    while (deadline.hasTimeLeft) {
      val channel = client.getChannel("mychannel")

      // transaction proposal
      val tpr = client.newTransactionProposalRequest()
      val cid = ChaincodeID.newBuilder().setName("mycc").build()
      tpr.setChaincodeID(cid)

      // cc function and args
      tpr.setFcn("set")
      tpr.setArgs("a", Random.nextInt(100).toString)

      // send trans proposal
      val res = channel.sendTransactionProposal(tpr)
      if (res.asScala.count(p => p.isInvalid) > 0) {
        println("Error proposal response")
      } else {
        // handle invoke response
        val event = channel.sendTransaction(res)
        event.whenCompleteAsync {
          new BiConsumer[BlockEvent#TransactionEvent, Throwable] {
            override def accept(t: BlockEvent#TransactionEvent, u: Throwable): Unit = {
              if (t.isValid) {
                println(s"Success response for ${t.getTransactionID}")
                j = j + 1
              } else {
                println(s"Error response for ${t.getTransactionID}")
              }
            }
          }
        }
        i = i + 1
      }
    }

    println(s"Invoked - $i")
    println(s"Success - $j")
  }

}
