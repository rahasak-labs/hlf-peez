package com.score.peez.hlf

import org.hyperledger.fabric.sdk.{Enrollment, User}

case class PeezUser(name: String, account: String, affiliation: String, mspId: String, enrollment: Enrollment) extends User {

  override def getName = name

  override def getRoles = null

  override def getAccount = account

  override def getAffiliation = affiliation

  override def getEnrollment = enrollment

  override def getMspId = mspId
}

