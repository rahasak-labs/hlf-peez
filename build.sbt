name := "peez"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {

  val akkaVersion = "2.3.15"
  val slickVersion = "3.0.0"

  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
    "com.typesafe.slick" %% "slick" % slickVersion,
    "org.scalaz" %% "scalaz-core" % "7.2.15",
    "org.slf4j" % "slf4j-api" % "1.7.5",
    "ch.qos.logback" % "logback-classic" % "1.0.9",
    "org.hyperledger.fabric-sdk-java" % "fabric-sdk-java" % "1.1.0",
    "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test"
  )
}

resolvers ++= Seq(
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
)
